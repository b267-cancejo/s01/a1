<?php

function fullAddress($street, $barangay, $municipality, $province, $country){
	return "$street, $barangay, $municipality, $province, $country";
}

function getLetterGrade($grade){
	if ($grade >= 98 && $grade < 101){
		return "A+";
	}
	else if($grade >= 95 && $grade < 98){
		return "A";
	}
	else if($grade >= 92 && $grade < 95){
		return "A-";
	}
	else if($grade >= 89 && $grade < 92){
		return "B+";
	}
	else if($grade >= 86 && $grade < 89){
		return "B";
	}
	else if($grade >= 83 && $grade < 86){
		return "B-";
	}
	else if($grade >= 80 && $grade < 83){
		return "C+";
	}
	else if($grade >= 77 && $grade < 80){
		return "C";
	}
	else if($grade >= 75 && $grade < 77){
		return "C-";
	}
	else{
		return "F";
	}
}